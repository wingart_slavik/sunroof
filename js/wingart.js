
var tabsBox = $(".tabs"),
	carousel = $('.carousel'),
	rotateSection = $(".rotate-elem");

if(tabsBox.length){
  include("js/easyResponsiveTabs.js");
}

if (carousel.length){
	include("js/owl.carousel.js");
}

if ($('.scroll-pane').length){
	include("js/jquery.mousewheel.js");
	include("js/jquery.jscrollpane.min.js");
}

if ($("#calculate-power").length){
	include("js/ion.rangeSlider.js");	
}

if (rotateSection.length){
	include("js/propeller.js");
}




function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(document).ready(function(){

	if ($('.scroll-pane').length){
		$('.scroll-pane').jScrollPane();
	}

	$("a.ankor").click(function() {
		var elementClick = $(this).attr("href")
		var destination = $(elementClick).offset().top;
		jQuery("html:not(:animated),body:not(:animated)").animate({
		scrollTop: destination
		}, 1800);
		return false;
	});

	
	// responsive navigation toggle button
	$("#resp-toggle-btn").on('click', function(){
	  	$("body").toggleClass('navTrue');
	})

	// init of tabs
	if (tabsBox.length){
		tabsBox.each(function(){
			var currentTabs = $(this);

			currentTabs.easyResponsiveTabs();
		})
	}

	// init of carousel
  	if (carousel.length){
	  	carousel.each(function(){
	  		var currentCarousel = $(this),
	  			items 			= currentCarousel.data('items'),
	  			singleItem      = false,
	  			itemsDesc 		= currentCarousel.data('items-desc'),
	  			itemsDescSm		= currentCarousel.data('items-desc-sm'),
	  			itemsTab 		= currentCarousel.data('items-tab');

	  		if (items < 1){
	  			return false;
	  		}

	  		if (items == 1){
	  			singleItem = 1;
	  			itemsDesc = 1;
		  			itemsDescSm = 1;
	  			itemsTab = 1;
	  		}

	  		currentCarousel.owlCarousel({
	  			items : items,
	  			singleItem : singleItem,
	  			itemsDesktop : [1199, itemsDesc],
		        itemsDesktopSmall : [979, itemsDescSm],
		        itemsTablet : [768, itemsTab],
		        itemsTabletSmall : false,
		        itemsMobile : [479, 1],
		        navigation : true,
		        navigationText : [" ", " "],
		        slideSpeed : 1200,
		        autoHeight : true
	  		})
	  	})
	}


	if ($("#calculate-power").length){
		var rangeSLider = $("#calculate-power");

		rangeSLider.ionRangeSlider({
		    type: "single",
		    min: 0,
		    max: 50,
		    from: 10,
		    keyboard: true
		});

		rangeSLider.on('change', function(){
			var $this = $(this),
		        value = $this.prop("value");
		    $("#power-val").text(value);
		})
	}

	if (rotateSection.length){
		rotateSection.each(function(){
			$(this).propeller({inertia: 0, speed: 0});
		})	
	}

	




})